package com.webappmate.trackemp;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.transition.Explode;
import android.transition.Transition;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.webappmate.trackemp.Dialogs.MyDialog;
import com.webappmate.trackemp.Service.CheckLocationServiceStatus;
import com.webappmate.trackemp.Service.LocationService;
import com.webappmate.trackemp.Service.MyLocation;
import com.webappmate.trackemp.Session.MySession;
import com.webappmate.trackemp.Session.SessionKeys;
import com.webappmate.trackemp.Utility.CheckServiceIsRunning;
import com.webappmate.trackemp.Utility.MyLog;
import com.webappmate.trackemp.Utility.Utils;
import com.webappmate.trackemp.model.TrackEmplyeeLocation;
import com.webappmate.trackemp.receivers.PushAlarmReceiver;
import com.webappmate.trackemp.route.AbstractRouting;
import com.webappmate.trackemp.route.Route;
import com.webappmate.trackemp.route.RouteException;
import com.webappmate.trackemp.route.Routing;
import com.webappmate.trackemp.route.RoutingListener;

import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;

public class HomePageActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener, GoogleMap.OnCameraChangeListener, GoogleMap.OnMyLocationChangeListener, RoutingListener {

    //protected fragment map;
    protected Button startTracking;
    // Check Menifest Permission in Marshmallow
    private static final int PERMISSION_REQUEST_CODE = 1;
    protected RelativeLayout contentHomePage;
    private GoogleMap googleMap;
    private MyLocation myLocation;
    private MySession mySession;
    private boolean mapCenter = true;
    private MyDialog myDialog;
    private float zoomLevel = 15;
    private List<LatLng> markerPoints;
    private List<TrackEmplyeeLocation> currentDateLocationList = new ArrayList<>();
    private String currentDate = "", gpsStatus = "", deviceId = "";
    private String waypoints = "waypoints=optimize:true|", url = "";
    private ProgressDialog progressDialog;
    protected LatLng start;
    protected LatLng end;

    private List<Polyline> polylines = new ArrayList<>();
    private static final int[] COLORS = new int[]{R.color.colorPrimaryDark, R.color.colorPrimary, R.color.primary_dark_material_light};


    private static final LatLngBounds BOUNDS_JAMAICA = new LatLngBounds(new LatLng(-57.965341647205726, 144.9987719580531),
            new LatLng(72.77492067739843, -9.998857788741589));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        getWindow().requestFeature(Window.FEATURE_ACTIVITY_TRANSITIONS);
        Animation();
        setContentView(R.layout.activity_home_page);
        initView();
    }

    /*Animation Method */

    public void Animation() {
        if (Build.VERSION.SDK_INT >= 21) {
            Transition trans = new Explode();
            trans.setDuration(700);
            getWindow().setEnterTransition(trans);
        }
    }

    private boolean checkPermission() {
        int accessFineLocation = ContextCompat.checkSelfPermission(HomePageActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
        int accessCourseLocation = ContextCompat.checkSelfPermission(HomePageActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION);
        int phoneState = ContextCompat.checkSelfPermission(HomePageActivity.this, Manifest.permission.READ_PHONE_STATE);
        if (accessFineLocation == PackageManager.PERMISSION_GRANTED) {
            if (accessCourseLocation == PackageManager.PERMISSION_GRANTED) {
                if (phoneState == PackageManager.PERMISSION_GRANTED) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }

        } else {
            return false;
        }
    }


    private void requestPermission() {
        ActivityCompat.requestPermissions(HomePageActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("value", "Permission Granted.");
                    setupData();
                } else {
                    Log.e("value", "Permission Denied.");
                }
                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.setOnCameraChangeListener(this);
        googleMap.setOnMyLocationChangeListener(this);
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkPermission()) {
                googleMap.setMyLocationEnabled(true);
            } else {
                requestPermission();
            }
        } else {
            googleMap.setMyLocationEnabled(true);
        }
        Location location = myLocation.getLocation();
        if (location != null) {
            userCurrentLocation(location.getLatitude(), location.getLongitude());
        }

       /* progressDialog = ProgressDialog.show(this, "Please wait.", "Fetching route information.", true);
        Routing routing = new Routing.Builder()
                .travelMode(AbstractRouting.TravelMode.DRIVING)
                .withListener(this)
                .alternativeRoutes(true)
                .waypoints(markerPoints)
                .build();
        routing.execute();*/
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.startTracking) {
            if (!CheckServiceIsRunning.isMyServiceRunning(LocationService.class, HomePageActivity.this)) {
                if (Build.VERSION.SDK_INT >= 23) {
                    if (checkPermission()) {
                        startTrackingService();
                    } else {
                        requestPermission();
                    }
                } else {
                    startTrackingService();
                }
            } else {
                setSnackBar(contentHomePage, "Tracker Already Running.");
            }
        }
    }

    private void initView() {
        // Initializing
        markerPoints = new ArrayList<LatLng>();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mySession = new MySession(this);
        myDialog = new MyDialog(this);
        startTracking = (Button) findViewById(R.id.startTracking);
        startTracking.setOnClickListener(HomePageActivity.this);
        contentHomePage = (RelativeLayout) findViewById(R.id.content_home_page);
        currentDate = Utils.getDate();
        currentDateLocationList = TrackEmplyeeLocation.getAllCurrentDateList(currentDate);
        for (TrackEmplyeeLocation emplyeeLocation : currentDateLocationList) {
            double lat = Double.parseDouble(emplyeeLocation.getLatitude());
            double lon = Double.parseDouble(emplyeeLocation.getLongitude());
            LatLng latLng = new LatLng(lat, lon);
            markerPoints.add(latLng);
            waypoints = waypoints + lat + "," + lon + "|";
        }

        setupData();
    }

    public void setupData() {
        myLocation = new MyLocation(this);
        myLocation.getLocation();
        if (!myLocation.checkGpsEnable()) {
            myLocation.showSettingsAlert();
            setSnackBar(contentHomePage, "Location Service Not Enable.");
        }
        if (myLocation.checkGpsEnable()) {
            gpsStatus = "1";
        } else {
            gpsStatus = "0";
        }
        if (mySession.getLoginReminder()) {
            startTrackingService();
        }
    }

    // Start the service
    public void startTrackingService() {
        if (!checkIfGooglePlayEnabled()) {
            return;
        }

        PushAlarmReceiver.setAlarm(HomePageActivity.this, Integer.parseInt(mySession.getIntervalTime()) * 1000 * 60);
        PushAlarmReceiver.setStartimeAlarm(HomePageActivity.this);
        if (!CheckServiceIsRunning.isMyServiceRunning(LocationService.class, HomePageActivity.this)) {
            startService(new Intent(HomePageActivity.this, LocationService.class));
        }
        if (!CheckServiceIsRunning.isMyServiceRunning(CheckLocationServiceStatus.class, HomePageActivity.this)) {
            startService(new Intent(HomePageActivity.this, CheckLocationServiceStatus.class));
        }
        deviceId = getImeino();
        mySession.SaveStringPref(SessionKeys.ImeiNo, deviceId);
        //PushAlarmReceiver.setStartimeAlarm(LoginScreenActivity.this);
    }

    public String getImeino() {
        TelephonyManager mngr = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        return mngr.getDeviceId();
    }

    private boolean checkIfGooglePlayEnabled() {
        if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(this) == ConnectionResult.SUCCESS) {
            return true;
        } else {
            Log.e("Home Page Track EMP :- ", "unable to connect to google play services.");
            Toast.makeText(getApplicationContext(), R.string.google_play_services_unavailable, Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public static void setSnackBar(View coordinatorLayout, String snackTitle) {
        Snackbar snackbar = Snackbar.make(coordinatorLayout, snackTitle, Snackbar.LENGTH_SHORT);
        snackbar.show();
        View view = snackbar.getView();
        TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        txtv.setGravity(Gravity.CENTER_HORIZONTAL);
    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        double cameraLatitude = cameraPosition.target.latitude;
        double cameraLongitude = cameraPosition.target.longitude;
        zoomLevel = googleMap.getCameraPosition().zoom;
        //Log.e("cameraLat & cameraLong", "" + cameraLatitude + ", " + cameraLongitude);
        if (mapCenter) {
            Location location = myLocation.getLocation();
            if (location != null) {
                mapCenter = false;
                userCurrentLocation(location.getLatitude(), location.getLongitude());
            }
        }
    }

    public void userCurrentLocation(Double latitude, Double longitude) {
        googleMap.clear();
        googleMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).title(""));
        CameraPosition cameraPos = CameraPosition.builder().target(new LatLng(latitude, longitude)).zoom(zoomLevel).tilt(10).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPos), 1000, null);
    }

    @Override
    public void onMyLocationChange(Location location) {
        if (location != null) {
            userCurrentLocation(location.getLatitude(), location.getLongitude());
        }
    }

    @Override
    public void onRoutingFailure(RouteException e) {
        progressDialog.dismiss();
        if (e != null) {
            Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Something went wrong, Try again", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRoutingStart() {

    }

    @Override
    public void onRoutingSuccess(List<Route> route, int shortestRouteIndex) {
        progressDialog.dismiss();
        CameraUpdate center = CameraUpdateFactory.newLatLng(start);
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(16);

        googleMap.moveCamera(center);

        if (polylines.size() > 0) {
            for (Polyline poly : polylines) {
                poly.remove();
            }
        }

        polylines = new ArrayList<>();
        //add route(s) to the map.
        for (int i = 0; i < route.size(); i++) {

            //In case of more than 5 alternative routes
            int colorIndex = i % COLORS.length;

            PolylineOptions polyOptions = new PolylineOptions();
            polyOptions.color(getResources().getColor(COLORS[colorIndex]));
            polyOptions.width(10 + i * 3);
            polyOptions.addAll(route.get(i).getPoints());
            Polyline polyline = googleMap.addPolyline(polyOptions);
            polylines.add(polyline);

            Toast.makeText(getApplicationContext(), "Route " + (i + 1) + ": distance - " + route.get(i).getDistanceValue() + ": duration - " + route.get(i).getDurationValue(), Toast.LENGTH_SHORT).show();
        }

        // Start marker
        MarkerOptions options = new MarkerOptions();
        options.position(start);
        options.icon(BitmapDescriptorFactory.fromResource(R.drawable.start_blue));
        googleMap.addMarker(options);

        // End marker
        options = new MarkerOptions();
        options.position(end);
        options.icon(BitmapDescriptorFactory.fromResource(R.drawable.end_green));
        googleMap.addMarker(options);
    }

    @Override
    public void onRoutingCancelled() {
        Log.i("Direction", "Routing was cancelled.");
    }
}
