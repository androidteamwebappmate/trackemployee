package com.webappmate.trackemp.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

@Table(name = "TrackEmplyeeLocation")
public class TrackEmplyeeLocation extends Model {

    @Column(name = "userLoginId")
    private String userLoginId;

    @Column(name = "userPassword")
    private String userPassword;

    @Column(name = "userAddress")
    private String userAddress;

    @Column(name = "deviceId")
    private String deviceId;

    @Column(name = "gpsStatus")
    private String gpsStatus;

    @Column(name = "latitude")
    private String latitude;

    @Column(name = "longitude")
    private String longitude;

    @Column(name = "captureTime")
    private String captureTime;

    @Column(name = "captureDate")
    private String captureDate;

    @Column(name = "oneTime")
    private boolean oneTime;

    @Column(name = "checkLocationStatus")
    private boolean checkLocationStatus;

    @Column(name = "mobileCellId")
    private String mobileCellId;

    @Column(name = "locationAreaCode")
    private String locationAreaCode;

    @Column(name = "mobileContryCode")
    private String mobileContryCode;

    @Column(name = "mobileNetworkCode")
    private String mobileNetworkCode;

    @Override
    public String toString() {
        return "TrackEmplyeeLocation{" +
                "userLoginId='" + userLoginId + '\'' +
                ", userPassword='" + userPassword + '\'' +
                ", userAddress='" + userAddress + '\'' +
                ", deviceId='" + deviceId + '\'' +
                ", gpsStatus='" + gpsStatus + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", captureTime='" + captureTime + '\'' +
                ", captureDate='" + captureDate + '\'' +
                ", oneTime=" + oneTime +
                ", checkLocationStatus=" + checkLocationStatus +
                ", mobileCellId='" + mobileCellId + '\'' +
                ", locationAreaCode='" + locationAreaCode + '\'' +
                ", mobileContryCode='" + mobileContryCode + '\'' +
                ", mobileNetworkCode='" + mobileNetworkCode + '\'' +
                '}';
    }

    public static List<TrackEmplyeeLocation> getAll() {
        return new Select().from(TrackEmplyeeLocation.class).execute();
    }

    public static List<TrackEmplyeeLocation> getAllOneTime() {
        return new Select().from(TrackEmplyeeLocation.class).where("oneTime = ?", false).execute();
    }

    public static List<TrackEmplyeeLocation> getAllCurrentDateList(String captureDate) {
        return new Select().from(TrackEmplyeeLocation.class).where("captureDate = ?", captureDate).execute();
    }

    public static TrackEmplyeeLocation getTrackValue(String trackId) {
        return new Select().from(TrackEmplyeeLocation.class).where("id = ?", trackId).executeSingle();
    }

    public String getUserLoginId() {
        return userLoginId;
    }

    public void setUserLoginId(String userLoginId) {
        this.userLoginId = userLoginId;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getGpsStatus() {
        return gpsStatus;
    }

    public void setGpsStatus(String gpsStatus) {
        this.gpsStatus = gpsStatus;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getCaptureTime() {
        return captureTime;
    }

    public void setCaptureTime(String captureTime) {
        this.captureTime = captureTime;
    }

    public String getCaptureDate() {
        return captureDate;
    }

    public void setCaptureDate(String captureDate) {
        this.captureDate = captureDate;
    }

    public boolean getOneTime() {
        return oneTime;
    }

    public void setOneTime(boolean oneTime) {
        this.oneTime = oneTime;
    }

    public boolean getCheckLocationStatus() {
        return checkLocationStatus;
    }

    public void setCheckLocationStatus(boolean checkLocationStatus) {
        this.checkLocationStatus = checkLocationStatus;
    }

    public String getMobileCellId() {
        return mobileCellId;
    }

    public void setMobileCellId(String mobileCellId) {
        this.mobileCellId = mobileCellId;
    }

    public String getLocationAreaCode() {
        return locationAreaCode;
    }

    public void setLocationAreaCode(String locationAreaCode) {
        this.locationAreaCode = locationAreaCode;
    }

    public String getMobileContryCode() {
        return mobileContryCode;
    }

    public void setMobileContryCode(String mobileContryCode) {
        this.mobileContryCode = mobileContryCode;
    }

    public String getMobileNetworkCode() {
        return mobileNetworkCode;
    }

    public void setMobileNetworkCode(String mobileNetworkCode) {
        this.mobileNetworkCode = mobileNetworkCode;
    }
}
