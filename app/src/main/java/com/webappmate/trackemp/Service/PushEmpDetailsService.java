package com.webappmate.trackemp.Service;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.webappmate.trackemp.Session.MySession;
import com.webappmate.trackemp.Session.SessionKeys;
import com.webappmate.trackemp.Slisteners.ResponceLisnter;
import com.webappmate.trackemp.Utility.CheckInternet;
import com.webappmate.trackemp.Utility.MyConstants;
import com.webappmate.trackemp.Utility.MyLog;
import com.webappmate.trackemp.Utility.MyToast;
import com.webappmate.trackemp.Utility.Utils;
import com.webappmate.trackemp.Volley.RequestModel;
import com.webappmate.trackemp.Volley.WysRequests;
import com.webappmate.trackemp.Webservices.WebKeys;
import com.webappmate.trackemp.Webservices.WebUrls;
import com.webappmate.trackemp.model.TrackEmplyeeLocation;

import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by ravi on 22/8/16.
 */
public class PushEmpDetailsService {
    private WysRequests wysRequests;
    private Context context;
    private MySession mySession;
    private String gpsStatus = "";
    private int pushInervalDelay;
    private double Latitude, Longitude;
    private boolean locationServiceStatus = false;

    public PushEmpDetailsService(Context context, double Latitude, double Longitude, boolean locationServiceStatus) {
        this.context = context;
        wysRequests = new WysRequests();
        mySession = new MySession(context);
        this.Latitude = Latitude;
        this.Longitude = Longitude;
        this.locationServiceStatus = locationServiceStatus;
        pushInervalDelay = Integer.parseInt(mySession.getIntervalTime()) * 1000 * 60;
        MyLog.ShowILog("Timer", pushInervalDelay + "");
    }

    public void getUserLatLong() {

        int deviceTime = Integer.parseInt(Utils.getHourTime());
        int startTime = Integer.parseInt(mySession.getStartTime());
        int endTime = Integer.parseInt(mySession.getEndTime());

        if (startTime <= deviceTime) {
            if (deviceTime <= endTime) {

                if (checkGpsEnable()) {
                    gpsStatus = "1";
                } else {
                    gpsStatus = "0";
                }

                if (locationServiceStatus) {
                    TrackEmplyeeLocation trackEmplyeeLocation = new TrackEmplyeeLocation();
                    trackEmplyeeLocation.setLatitude(Latitude + "");
                    trackEmplyeeLocation.setLongitude(Longitude + "");
                    trackEmplyeeLocation.setGpsStatus(gpsStatus);
                    trackEmplyeeLocation.setDeviceId(mySession.getImeino());
                    trackEmplyeeLocation.setUserLoginId(mySession.getLoginId());
                    trackEmplyeeLocation.setUserPassword(mySession.getPassword());
                    trackEmplyeeLocation.setUserAddress("");
                    trackEmplyeeLocation.setCheckLocationStatus(locationServiceStatus);
                    trackEmplyeeLocation.setCaptureTime(Utils.getTime());
                    trackEmplyeeLocation.setCaptureDate(Utils.getDate());
                    trackEmplyeeLocation.setOneTime(false);
                    trackEmplyeeLocation.setLocationAreaCode("");
                    trackEmplyeeLocation.setMobileCellId("");
                    trackEmplyeeLocation.setMobileContryCode("");
                    trackEmplyeeLocation.setMobileNetworkCode("");
                    trackEmplyeeLocation.save();
                }

                if (CheckInternet.isNetwork(context)) {
                    if (mySession.getLoginReminder()) {
                        getServerDateTime();
                        updateLocalTrackLocation();
                    }
                }

                MyLog.ShowILog("LatLong", Longitude + "\n" + Longitude);
            }
        }
    }

    public boolean checkGpsEnable() {
        LocationManager locationManager = (LocationManager) context.getSystemService(context.LOCATION_SERVICE);
        boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        return isGPSEnabled;
    }

    private void getServerDateTime() {
        if (CheckInternet.isNetwork(context)) {
            ArrayList<RequestModel> list = new ArrayList<>();
            //list.add(new RequestModel(WebKeys.Key_UserId, mySession.getUserId()));
            wysRequests.MakeStrRequest(WebKeys.Tag_ServerDateTime, WebUrls.ServerDateTime, list, responceLisnter);
        } else {
            mySession.SaveStringPref(SessionKeys.trackTime, Utils.getTime());
            mySession.SaveStringPref(SessionKeys.trackDate, Utils.getDate());
        }
    }

    private boolean checkPermission() {
        int accessFineLocation = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);
        int accessCourseLocation = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION);
        int phoneState = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE);
        if (accessFineLocation == PackageManager.PERMISSION_GRANTED) {
            if (accessCourseLocation == PackageManager.PERMISSION_GRANTED) {
                if (phoneState == PackageManager.PERMISSION_GRANTED) {
                    return true;
                } else {
                    Toast.makeText(context, "TrackEMP \nPlease Provide permission for Phone", Toast.LENGTH_LONG).show();
                    MyLog.ShowLog(MyConstants.Result, "TrackEMP \nPlease Provide permission for Phone");
                    return false;
                }
            } else {
                Toast.makeText(context, "TrackEMP \nPlease Provide permission for Location", Toast.LENGTH_LONG).show();
                MyLog.ShowLog(MyConstants.Result, "TrackEMP \nPlease Provide permission for Location");
                return false;
            }
        } else {
            Toast.makeText(context, "TrackEMP \nPlease Provide permission for Location", Toast.LENGTH_LONG).show();
            MyLog.ShowLog(MyConstants.Result, "TrackEMP \nPlease Provide permission for Location");
            return false;
        }
    }

    public void updateLocalTrackLocation() {
        List<TrackEmplyeeLocation> trackEmpLocations = new ArrayList<TrackEmplyeeLocation>();
        //trackEmpLocations = TrackEmplyeeLocation.getAll();
        trackEmpLocations = TrackEmplyeeLocation.getAllOneTime();
        for (TrackEmplyeeLocation trackEmpLocation : trackEmpLocations) {
            if (trackEmpLocation.getCheckLocationStatus()) {
                getUserLocation(trackEmpLocation);
            } else {
                GeoLocationData(trackEmpLocation);
            }
        }
    }

    public void getUserLocation(TrackEmplyeeLocation trackEmpLocation) {
        if (!trackEmpLocation.getOneTime()) {

            String result = "";
            Geocoder gc = new Geocoder(context, Locale.getDefault());
            try {
                List<Address> addressList = gc.getFromLocation(Double.parseDouble(trackEmpLocation.getLatitude()), Double.parseDouble(trackEmpLocation.getLongitude()), 1);
                if (addressList != null && addressList.size() > 0) {
                    Address address = addressList.get(0);
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                        sb.append(address.getAddressLine(i)).append("\n");
                    }
                    sb.append(address.getLocality()).append("\n");
                    sb.append(address.getPostalCode()).append("\n");
                    sb.append(address.getCountryName());
                    result = sb.toString();
                    if (result == null) {
                        result = "Unable to get address.";
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                result = "Unable to get address.";
            }
            if (mySession.getLoginReminder()) {
                if (Build.VERSION.SDK_INT >= 23) {
                    if (checkPermission()) {
                        Log.e("value", "Permission already Granted.");
                        PushStoreTrackLoc(trackEmpLocation, result);
                    }
                } else {
                    Log.e("value", "Not required for requesting runtime permission");
                    PushStoreTrackLoc(trackEmpLocation, result);
                }
            }
        }
    }

    private void PushStoreTrackLoc(TrackEmplyeeLocation trackEmpLocation, String locationAddress) {
        if (locationAddress.equalsIgnoreCase("")) {
            locationAddress = "Unable to get address.";
        }
        if (CheckInternet.isNetwork(context)) {
            ArrayList<RequestModel> list = new ArrayList<>();
            list.add(new RequestModel(WebKeys.push_lat, trackEmpLocation.getLatitude()));
            list.add(new RequestModel(WebKeys.push_long, trackEmpLocation.getLongitude()));
            list.add(new RequestModel(WebKeys.push_gpsstatus, trackEmpLocation.getGpsStatus()));
            list.add(new RequestModel(WebKeys.push_device_id, trackEmpLocation.getDeviceId()));
            list.add(new RequestModel(WebKeys.push_login_id, trackEmpLocation.getUserLoginId()));
            list.add(new RequestModel(WebKeys.push_password, trackEmpLocation.getUserPassword()));
            list.add(new RequestModel(WebKeys.push_trackAddress, locationAddress));
            list.add(new RequestModel(WebKeys.push_trackTime, trackEmpLocation.getCaptureTime()));
            list.add(new RequestModel(WebKeys.push_trackDate, trackEmpLocation.getCaptureDate()));
            wysRequests.MakeStrRequest(trackEmpLocation.getId() + "", WebUrls.push_empdetails, list, responceLisnter);
        }
    }

    ResponceLisnter responceLisnter = new ResponceLisnter() {
        @Override
        public void getResponce(String response, String Tag) {
            MyLog.ShowLog(MyConstants.Result, response);
            if (Tag.equalsIgnoreCase(WebKeys.Tag_ServerDateTime)) {
                serverDateTime(response);
            } else {
                PushEmpStoreResult(response, Tag);
            }
        }

        @Override
        public void getResponceError(String errorStr, String Tag) {
            MyToast.Smsg(context, errorStr);
        }
    };

    private void serverDateTime(String result) {
        if (result != null) {
            Log.e("Server DateTime Respon", result);
            try {
                JSONObject jsonObject = new JSONObject(result);
                String DateTimeTaken = jsonObject.getString("time");
                String currentDateTime = jsonObject.getString("date");
                mySession.SaveStringPref(SessionKeys.trackTime, DateTimeTaken);
                mySession.SaveStringPref(SessionKeys.trackDate, currentDateTime);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void PushEmpStoreResult(String response, String Tag) {
        try {
            JSONObject obj = new JSONObject(response);
            String status = obj.optString(WebKeys.Status);
            if (status.equals("1")) {
                String startTime = obj.optString("startTime");
                String endTime = obj.optString("endTime");
                String serverTime = obj.optString("serverTime");
                String interval = obj.optString("Interval");

                mySession.SaveStringPref(SessionKeys.StartTime, startTime);
                mySession.SaveStringPref(SessionKeys.EndTime, endTime);
                mySession.SaveStringPref(SessionKeys.ServerTime, serverTime);
                mySession.SaveStringPref(SessionKeys.IntervalTime, interval);

                TrackEmplyeeLocation emplyeeLocation = TrackEmplyeeLocation.getTrackValue(Tag);
                emplyeeLocation.setOneTime(true);
                emplyeeLocation.save();
                //emplyeeLocation.delete();
            } else {
                TrackEmplyeeLocation emplyeeLocation = TrackEmplyeeLocation.getTrackValue(Tag);
                emplyeeLocation.setOneTime(false);
                emplyeeLocation.save();
            }
        } catch (Exception e) {
            e.printStackTrace();
            TrackEmplyeeLocation emplyeeLocation = TrackEmplyeeLocation.getTrackValue(Tag);
            emplyeeLocation.setOneTime(false);
            emplyeeLocation.save();
        }
    }

    public void GeoLocationData(final TrackEmplyeeLocation trackEmplyeeLocation) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    // 1. create HttpClient
                    HttpClient httpclient = new DefaultHttpClient();

                    // 2. make POST request to the given URL
                    HttpPost httpPost = new HttpPost("https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyAI4cV8uZ9w_uRzfmDRWzKmU54L86Jv98U");

                    String json = "";
                    // 3. build jsonObject
                    JSONObject cellTowerObject = new JSONObject();
                    cellTowerObject.accumulate("cellId", trackEmplyeeLocation.getMobileCellId());
                    cellTowerObject.accumulate("locationAreaCode", trackEmplyeeLocation.getLocationAreaCode());
                    cellTowerObject.accumulate("mobileCountryCode", trackEmplyeeLocation.getMobileContryCode());
                    cellTowerObject.accumulate("mobileNetworkCode", trackEmplyeeLocation.getMobileNetworkCode());
                    JSONArray cellTowersArray = new JSONArray();
                    cellTowersArray.put(cellTowerObject);
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.accumulate("cellTowers", cellTowersArray);

                    // 4. convert JSONObject to JSON to String
                    json = jsonObject.toString();

                    // 5. set json to StringEntity
                    StringEntity se = new StringEntity(json);
                    se.setContentType("application/json");
                    // 6. set httpPost Entity
                    httpPost.setEntity(se);

                    // 7. Set some headers to inform server about the type of the content
                    httpPost.setHeader("Accept", "application/json");
                    httpPost.setHeader("Content-type", "application/json");

                    ResponseHandler<String> responseHandler = new BasicResponseHandler();
                    String response = httpclient.execute(httpPost, responseHandler);

                    JSONObject jsonResult = new JSONObject(response);
                    JSONObject location = jsonResult.getJSONObject("location");
                    double lat, lng;
                    lat = location.getDouble("lat");
                    lng = location.getDouble("lng");
                    Log.e("User Location : - ", "Latitude : - " + lat + "  Longitude :- " + lng);

                    trackEmplyeeLocation.setLatitude(lat + "");
                    trackEmplyeeLocation.setLongitude(lng + "");

                    getUserLocation(trackEmplyeeLocation);
                    /*PushEmpDetailsService pushEmpDetailsService = new PushEmpDetailsService(context, lat, lng);
                    pushEmpDetailsService.getUserLatLong();*/

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }).start();
    }

}
