package com.webappmate.trackemp.Service;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.telephony.CellLocation;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.webappmate.trackemp.Session.MySession;
import com.webappmate.trackemp.Utility.CheckServiceIsRunning;
import com.webappmate.trackemp.Utility.MyConstants;
import com.webappmate.trackemp.Utility.MyLog;
import com.webappmate.trackemp.Utility.Utils;
import com.webappmate.trackemp.model.TrackEmplyeeLocation;

public class CheckLocationServiceStatus extends Service {
    private static final String TAG = "Location Service Status";
    private Handler handler;
    private MySession mySession;
    private Context context;
    private double Latitude, Longitude;
    private String mobileContryCode, mobileNetworkCode;
    private String mobileCellId, locationAreaCode;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mySession = new MySession(this);
        context = this;
        handler = new Handler();
        handler.postDelayed(runnable, Integer.parseInt(mySession.getIntervalTime()) * 1000 * 60);
        return START_STICKY;
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {

            boolean locationServiceStatus = isLocationEnabled(context);
            Log.d(TAG, "" + locationServiceStatus);
            if (locationServiceStatus) {
                if (!CheckServiceIsRunning.isMyServiceRunning(LocationService.class, context)) {
                    context.startService(new Intent(context, LocationService.class));
                }
            } else {
                if (Build.VERSION.SDK_INT >= 23) {
                    if (checkPermission()) {
                        Log.e("value", "Permission already Granted.");
                        getTelephonicDetails();
                    }
                } else {
                    Log.e("value", "Not required for requesting runtime permission");
                    getTelephonicDetails();
                }
            }

            handler.postDelayed(runnable, Integer.parseInt(mySession.getIntervalTime()) * 1000 * 60);
        }
    };

    private boolean checkPermission() {
        int accessFineLocation = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);
        int accessCourseLocation = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION);
        int phoneState = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE);
        if (accessFineLocation == PackageManager.PERMISSION_GRANTED) {
            if (accessCourseLocation == PackageManager.PERMISSION_GRANTED) {
                if (phoneState == PackageManager.PERMISSION_GRANTED) {
                    return true;
                } else {
                    Toast.makeText(context, "TrackEMP \nPlease Provide permission for Phone", Toast.LENGTH_LONG).show();
                    MyLog.ShowLog(MyConstants.Result, "TrackEMP \nPlease Provide permission for Phone");
                    return false;
                }
            } else {
                Toast.makeText(context, "TrackEMP \nPlease Provide permission for Geo Location", Toast.LENGTH_LONG).show();
                MyLog.ShowLog(MyConstants.Result, "TrackEMP \nPlease Provide permission for eo GLocation");
                return false;
            }
        } else {
            Toast.makeText(context, "TrackEMP \nPlease Provide permission for Location", Toast.LENGTH_LONG).show();
            MyLog.ShowLog(MyConstants.Result, "TrackEMP \nPlease Provide permission for Location");
            return false;
        }
    }

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;
        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    public void getTelephonicDetails() {
        try {
            TelephonyManager tel = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            if (tel != null) {
                CellLocation loc = tel.getCellLocation();
                if ((loc != null) && (loc instanceof GsmCellLocation)) {
                    GsmCellLocation gsmLoc = (GsmCellLocation) loc;
                    String op = tel.getNetworkOperator();

                    mobileCellId = "" + gsmLoc.getCid();
                    locationAreaCode = "" + gsmLoc.getLac();
                    mobileContryCode = op.substring(0, 3);
                    mobileNetworkCode = op.substring(3);
                    Log.e("Telephonic : - ", mobileCellId + " " + mobileNetworkCode + " " + mobileContryCode + " " + locationAreaCode);

                    int deviceTime = Integer.parseInt(Utils.getHourTime());
                    int startTime = Integer.parseInt(mySession.getStartTime());
                    int endTime = Integer.parseInt(mySession.getEndTime());

                    if (startTime <= deviceTime) {
                        if (deviceTime <= endTime) {
                            TrackEmplyeeLocation trackEmplyeeLocation = new TrackEmplyeeLocation();
                            trackEmplyeeLocation.setLatitude("");
                            trackEmplyeeLocation.setLongitude("");
                            trackEmplyeeLocation.setGpsStatus("0");
                            trackEmplyeeLocation.setDeviceId(mySession.getImeino());
                            trackEmplyeeLocation.setUserLoginId(mySession.getLoginId());
                            trackEmplyeeLocation.setUserPassword(mySession.getPassword());
                            trackEmplyeeLocation.setUserAddress("");
                            trackEmplyeeLocation.setCheckLocationStatus(false);
                            trackEmplyeeLocation.setCaptureTime(Utils.getTime());
                            trackEmplyeeLocation.setCaptureDate(Utils.getDate());
                            trackEmplyeeLocation.setOneTime(false);
                            trackEmplyeeLocation.setLocationAreaCode(locationAreaCode);
                            trackEmplyeeLocation.setMobileCellId(mobileCellId);
                            trackEmplyeeLocation.setMobileContryCode(mobileContryCode);
                            trackEmplyeeLocation.setMobileNetworkCode(mobileNetworkCode);
                            trackEmplyeeLocation.save();
                        }
                    }

                    PushEmpDetailsService pushEmpDetailsService = new PushEmpDetailsService(this, Latitude, Longitude, false);
                    pushEmpDetailsService.getUserLatLong();

                } else {
                    Toast.makeText(context, "No valid GSM network found", Toast.LENGTH_LONG).show();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (!CheckServiceIsRunning.isMyServiceRunning(CheckLocationServiceStatus.class, this)) {
            startService(new Intent(this, CheckLocationServiceStatus.class));
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }
}
