package com.webappmate.trackemp.Session;

public class SessionKeys {

    public static String UserId = "UserId";
    public static String Name = "Name";
    public static String Email = "Email";
    public static String Mobile = "Mobile";
    public static String GcmId = "GcmId";
    public static String ImeiNo = "ImeiNo";
    public static String Password = "password";
    public static String LoginId = "login_id";
    public static String LoginReminder = "Reminder";
    public static String StartTime = "startTime";
    public static String EndTime = "endTime";
    public static String ServerTime = "serverTime";
    public static String IntervalTime = "Interval";
    public static String DutyStatus = "dutystatus";

    public static String trackTime = "captureTime";
    public static String trackDate = "captureDate";

    public static String latitude = "latitude";
    public static String longitude = "longitude";

    public static String MobileCellId = "cellId";
    public static String LocationAreaCode = "locationAreaCode";
    public static String MobileCountryCode = "mobileCountryCode";
    public static String MobileNetworkCode = "mobileNetworkCode";

}
