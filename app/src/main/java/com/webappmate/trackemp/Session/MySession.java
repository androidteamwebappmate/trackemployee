package com.webappmate.trackemp.Session;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class MySession {

    SharedPreferences pref;
    Editor editor;

    public MySession(Context context) {
        pref = context.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
    }

    public void SaveStringPref(String Key, String Value) {
        editor = pref.edit();
        editor.putString(Key, Value);
        editor.commit();
    }

    public void SaveIntPref(String Key, int Value) {
        editor = pref.edit();
        editor.putInt(Key, Value);
        editor.commit();
    }

    public void SaveFloatPref(String Key, float Value) {
        editor = pref.edit();
        editor.putFloat(Key, Value);
        editor.commit();
    }

    public void SaveDoublePref(String Key, long Value) {
        editor = pref.edit();
        editor.putLong(Key, Value);
        editor.commit();
    }

    public void SaveBooleanPref(String Key, boolean Value) {
        editor = pref.edit();
        editor.putBoolean(Key, Value);
        editor.commit();
    }

    public double getLatitude() {
        return pref.getLong(SessionKeys.latitude, 0);
    }

    public double getLongitude() {
        return pref.getLong(SessionKeys.longitude, 0);
    }

    public String getUserId() {
        return pref.getString(SessionKeys.UserId, "");
    }

    public String getName() {
        return pref.getString(SessionKeys.Name, "");
    }

    public String getEmail() {
        return pref.getString(SessionKeys.Email, "");
    }

    public String getMobile() {
        return pref.getString(SessionKeys.Mobile, "");
    }

    public String getGcmId() {
        return pref.getString(SessionKeys.GcmId, "");
    }

    public String getImeino() {
        return pref.getString(SessionKeys.ImeiNo, "ABC");
    }

    public String getPassword() {
        return pref.getString(SessionKeys.Password, "");
    }

    public String getLoginId() {
        return pref.getString(SessionKeys.LoginId, "");
    }

    public boolean getLoginReminder() {
        return pref.getBoolean(SessionKeys.LoginReminder, false);
    }

    public String getStartTime() {
        return pref.getString(SessionKeys.StartTime, "");
    }

    public String getEndTime() {
        return pref.getString(SessionKeys.EndTime, "");
    }

    public String getServerTime() {
        return pref.getString(SessionKeys.ServerTime, "");
    }

    public String getIntervalTime() {
        return pref.getString(SessionKeys.IntervalTime, "");
    }

    public String getDutyStatus() {
        return pref.getString(SessionKeys.DutyStatus, "");
    }

    public String getTrakerTime() {
        return pref.getString(SessionKeys.trackTime, "");
    }

    public String getTrakerDate() {
        return pref.getString(SessionKeys.trackDate, "");
    }


    public int getMobileCellId() {
        return pref.getInt(SessionKeys.MobileCellId, 0);
    }

    public int getLocationAreaCode() {
        return pref.getInt(SessionKeys.LocationAreaCode, 0);
    }

    public int getMobileCountryCode() {
        return pref.getInt(SessionKeys.MobileCountryCode, 0);
    }

    public int getMobileNetworkCode() {
        return pref.getInt(SessionKeys.MobileNetworkCode, 0);
    }

}
