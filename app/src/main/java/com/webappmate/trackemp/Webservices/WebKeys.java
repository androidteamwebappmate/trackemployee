package com.webappmate.trackemp.Webservices;

public class WebKeys {

    // Request for which api request identifier.
    public static String push_key = "empdetails";
    public static String push_StoreLocalkey = "StoreLocal";
    public static String Tag_ServerDateTime = "ServerDateTime";

    public static String push_lat = "lat";
    public static String push_long = "long";
    public static String push_gpsstatus = "gpsstatus";
    public static String push_device_id = "device_id";
    public static String push_login_id = "login_id";
    public static String push_password = "password";
    public static String push_trackAddress = "trackAddress";
    public static String push_trackTime = "captureTime";
    public static String push_trackDate = "captureDate";
    //public static String push_key = "empdetails";

    public static String Status = "status";
    public static String Headerkey = "key";
    public static String HeaderValue = "nG97Ui383663cBeF0O5foE46WkBOfGEB";

    public static String MobileCellId = "cellId";
    public static String LocationAreaCode = "locationAreaCode";
    public static String MobileCountryCode = "mobileCountryCode";
    public static String MobileNetworkCode = "mobileNetworkCode";


}
