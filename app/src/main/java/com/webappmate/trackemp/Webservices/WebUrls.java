package com.webappmate.trackemp.Webservices;

public class WebUrls {

    //Test URL1
    //public static String BaseUrl = "http://tracker.webappmate.in/json/";
    //Live Url http://emptracker.zulu.in/
    public static String BaseUrl = "http://emptracker.zulu.in/json/";


    public static String push_empdetails = BaseUrl + "track_emp.php";
    public static String ServerDateTime = BaseUrl + "date_time_responce.php";

    // https://www.googleapis.com/geolocation/v1/geolocate?key=YOUR_API_KEY
    public static String GeoLocationAPI = BaseUrl + "https://www.googleapis.com/geolocation/v1/geolocate?key=";
}
