package com.webappmate.trackemp;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.crashlytics.android.Crashlytics;
import com.webappmate.trackemp.Session.MySession;
import com.webappmate.trackemp.Session.SessionKeys;

import io.fabric.sdk.android.Fabric;


public class SplashActivity extends AppCompatActivity {

    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;
    private MySession mySession;
    private boolean checkUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash);
        mySession = new MySession(SplashActivity.this);
        checkUser = mySession.getLoginReminder();
        if (!mySession.getLoginReminder()) {
            mySession.SaveStringPref(SessionKeys.StartTime, "07");
            mySession.SaveStringPref(SessionKeys.EndTime, "20");
            mySession.SaveStringPref(SessionKeys.ServerTime, "");
            mySession.SaveStringPref(SessionKeys.IntervalTime, "10");
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (checkUser) {
                    Intent homePaheIntent = new Intent(SplashActivity.this, HomePageActivity.class);
                    startActivity(homePaheIntent);
                    overridePendingTransition(R.anim.in_left, R.anim.out_left);
                    finish();
                } else {
                    Intent splashIntent = new Intent(SplashActivity.this, LoginScreenActivity.class);
                    startActivity(splashIntent);
                    overridePendingTransition(R.anim.in_left, R.anim.out_left);
                    finish();
                }
            }
        }, AUTO_HIDE_DELAY_MILLIS);
    }

}
