package com.webappmate.trackemp.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.webappmate.trackemp.Service.CheckLocationServiceStatus;
import com.webappmate.trackemp.Service.LocationService;
import com.webappmate.trackemp.Session.MySession;

public class BootCompletedReceiver extends BroadcastReceiver {

    private static final String TAG = "Track EMP";
    private MySession mySession;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e(TAG, "onReceive Start");
        mySession = new MySession(context);
        PushAlarmReceiver.setAlarm(context, Integer.parseInt(mySession.getIntervalTime()) * 1000 * 60);
        PushAlarmReceiver.setStartimeAlarm(context);
        context.startService(new Intent(context, LocationService.class));
        context.startService(new Intent(context, CheckLocationServiceStatus.class));
        Toast.makeText(context, "SetAlarm", Toast.LENGTH_LONG).show();
        Log.e(TAG, "SetAlarm Boot Completed");
    }

}
