package com.webappmate.trackemp.receivers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import com.webappmate.trackemp.Service.CheckLocationServiceStatus;
import com.webappmate.trackemp.Service.LocationService;
import com.webappmate.trackemp.Utility.CheckServiceIsRunning;
import com.webappmate.trackemp.Utility.MyToast;

import java.util.Calendar;

public class PushAlarmReceiver extends WakefulBroadcastReceiver {

    private static final String TAG = "Track EMP";
    private static final String alarmAction = "startService";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e(TAG, "onReceive Start");

        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "");
        wl.acquire();
        if (!CheckServiceIsRunning.isMyServiceRunning(LocationService.class, context)) {
            context.startService(new Intent(context, LocationService.class));
        }
        if (!CheckServiceIsRunning.isMyServiceRunning(CheckLocationServiceStatus.class, context)) {
            context.startService(new Intent(context, CheckLocationServiceStatus.class));
        }
        wl.release();
    }

    public static void setAlarm(Context context, long intervalTime) {
        try {
            AlarmManager mAlarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent(context, PushAlarmReceiver.class);
            intent.setAction(alarmAction);
            PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            mAlarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), intervalTime, pi);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void setStartimeAlarm(Context context) {
        try {
            AlarmManager mAlarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent(context, PushAlarmReceiver.class);
            intent.setAction(alarmAction);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

            Calendar alarmStartTime = Calendar.getInstance();
            Calendar now = Calendar.getInstance();
            alarmStartTime.set(Calendar.HOUR_OF_DAY, 7);
            alarmStartTime.set(Calendar.MINUTE, 00);
            alarmStartTime.set(Calendar.SECOND, 0);
            if (now.after(alarmStartTime)) {
                Log.d("Hey", "Added a day");
                alarmStartTime.add(Calendar.DATE, 1);
            }

            mAlarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, alarmStartTime.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
            Log.d("Alarm", "Alarms set for everyday 7 am.");
            MyToast.Lmsg(context, "TrackEMP Start");

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
