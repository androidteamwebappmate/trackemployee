package com.webappmate.trackemp.Utility;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by ravi on 29/8/16.
 */
public class Utils {

    public static String getTime() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        String strDate = sdf.format(c.getTime());
        return strDate;
    }

    public static String getHourTime() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH");
        String strDate = sdf.format(c.getTime());
        return strDate;
    }

    public static String getDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String strDate = sdf.format(c.getTime());
        return strDate;
    }

}
