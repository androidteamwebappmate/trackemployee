package com.webappmate.trackemp.Utility;

import android.app.ActivityManager;
import android.content.Context;

/**
 * Created by ravi on 23/8/16.
 */
public class CheckServiceIsRunning {

    public static boolean isMyServiceRunning(Class<?> serviceClass, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
