package com.webappmate.trackemp.Utility;

import android.util.Log;

public class MyLog {

    public static void ShowLog(String Tag, String Msg) {
        Log.d(Tag, Msg);
    }

    public static void ShowELog(String Tag, String Msg) {
        Log.e(Tag, Msg);
    }

    public static void ShowILog(String Tag, String Msg) {
        Log.i(Tag, Msg);
    }
}
