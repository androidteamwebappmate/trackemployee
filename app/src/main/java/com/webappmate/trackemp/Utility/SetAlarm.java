package com.webappmate.trackemp.Utility;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.webappmate.trackemp.receivers.PushAlarmReceiver;

import java.util.Calendar;

/**
 * Created by ravi on 24/8/16.
 */
public class SetAlarm {

    public void setAlarm(Context context, int startTime) {
        try {
            Intent myIntent = new Intent(context, PushAlarmReceiver.class);
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, myIntent, 0);
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY, startTime);
            calendar.set(Calendar.MINUTE, 00);
            calendar.set(Calendar.SECOND, 00);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), 24 * 60 * 60 * 1000, pendingIntent); //Repeat every 24 hours

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
