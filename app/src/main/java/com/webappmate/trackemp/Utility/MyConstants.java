package com.webappmate.trackemp.Utility;

import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;

public class MyConstants {

    // BroadCast Keys.
    public static final String Reciever = "Reciever";
    public static final String Broadcast_SelfieStation = "broadcast_selfiestation";

    //Dialog Messages
    public static final String InternetMsg = "Please turn on Internet";
    public static final String InternetTitle = "Internet";
    public static final String DialogPosTitle = "Ok";

    // Page Keys
    public static final String Version = "Version : ";
    public static final String Folder_Frame = "Frames";
    public static final DecelerateInterpolator DECCELERATE_INTERPOLATOR = new DecelerateInterpolator();
    public static final AccelerateInterpolator ACCELERATE_INTERPOLATOR = new AccelerateInterpolator();
    public static final OvershootInterpolator OVERSHOOT_INTERPOLATOR = new OvershootInterpolator(4);

    // Log Messages
    public static String Directory_Log = "Directory was created";
    public static String Result = "Result";

    public static boolean GalleryImg = false;

    public static String ErrorKey = "ErrorKey";
    public static String Error_ViewFriend = "Error_ViewFriend";
    public static String Error_FrameView = "Error_FrameView";
    public static String Error_SelfiHub = "Error_SelfiHub";
    public static String Error_SelfiContest = "Error_SelfiHub";
    public static String Error_ConfessionView = "Error_ConfessionView";


    public static final String FIRST_TIME = "first_time";
    public static final String URL = "url";
    public static final String TOKEN = "token";
    public static final String DATASOURCE_VARIABLE = "location";
    public static final String VARIABLE_ID = "variable";
    public static final String PUSH_TIME = "push_time";
    public static final String SERVICE_RUNNING = "service_running";

    public static final int NOTIFICATION_ID = 1337;

}
