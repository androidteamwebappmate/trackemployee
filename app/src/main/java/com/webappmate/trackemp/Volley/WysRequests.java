package com.webappmate.trackemp.Volley;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.webappmate.trackemp.Slisteners.ResponceLisnter;
import com.webappmate.trackemp.TrackEMPApplication;
import com.webappmate.trackemp.Utility.MyLog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class WysRequests {

    public void MakeStrRequest(final String Tag, String url, final ArrayList<RequestModel> list, final ResponceLisnter responceLisnter) {
        MyLog.ShowLog("Url", url);

        StringRequest sr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                responceLisnter.getResponce(response, Tag);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    responceLisnter.getResponceError(VollyError.TIMEOUT_ERROR, Tag);
                } else if (error instanceof AuthFailureError) {
                    responceLisnter.getResponceError(VollyError.AUTH_ERROR, Tag);
                } else if (error instanceof ServerError) {
                    responceLisnter.getResponceError(VollyError.SERVER_ERROR, Tag);
                } else if (error instanceof NetworkError) {
                    responceLisnter.getResponceError(VollyError.NETWORK_ERROR, Tag);
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                if (list.size() > 0) {
                    for (int i = 0; i < list.size(); i++) {
                        MyLog.ShowLog(list.get(i).getKey(), list.get(i).getValue());
                        params.put(list.get(i).getKey(), list.get(i).getValue());
                    }
                }
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //params.put(WebKeys.Headerkey, WebKeys.HeaderValue);
                return params;
            }
        };
        TrackEMPApplication.getInstance().getRequestQueue().add(sr);
        sr.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // 25000 milisecond = 25 second
    }
}

