package com.webappmate.trackemp;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.telephony.TelephonyManager;
import android.transition.Explode;
import android.transition.Transition;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.webappmate.trackemp.Dialogs.MyDialog;
import com.webappmate.trackemp.Service.CheckLocationServiceStatus;
import com.webappmate.trackemp.Service.LocationService;
import com.webappmate.trackemp.Service.MyLocation;
import com.webappmate.trackemp.Session.MySession;
import com.webappmate.trackemp.Session.SessionKeys;
import com.webappmate.trackemp.Slisteners.ResponceLisnter;
import com.webappmate.trackemp.Utility.CheckInternet;
import com.webappmate.trackemp.Utility.CheckServiceIsRunning;
import com.webappmate.trackemp.Utility.LocationAddress;
import com.webappmate.trackemp.Utility.MyConstants;
import com.webappmate.trackemp.Utility.MyLog;
import com.webappmate.trackemp.Utility.MyToast;
import com.webappmate.trackemp.Utility.Utils;
import com.webappmate.trackemp.Volley.RequestModel;
import com.webappmate.trackemp.Volley.WysRequests;
import com.webappmate.trackemp.Webservices.WebKeys;
import com.webappmate.trackemp.Webservices.WebUrls;
import com.webappmate.trackemp.receivers.PushAlarmReceiver;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.fabric.sdk.android.Fabric;

public class LoginScreenActivity extends AppCompatActivity implements View.OnClickListener {
    // Check Menifest Permission in Marshmallow
    private static final int PERMISSION_REQUEST_CODE = 1;
    private MyLocation myLocation;
    WysRequests wysRequests = new WysRequests();
    private String gpsStatus = "";
    MySession mySession;
    private LinearLayout linearLayout;
    private EditText userIdEdit, userPassEdit;
    private TextInputLayout userIdTIL, userPassTIL;
    private AppCompatButton signUpCompatButton;
    private String deviceId = "";
    private RelativeLayout coordinatorLayout;
    private MyDialog myDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        getWindow().requestFeature(Window.FEATURE_ACTIVITY_TRANSITIONS);
        Animation();
        setContentView(R.layout.activity_login);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        mySession = new MySession(this);
        myDialog = new MyDialog(this);
        initilization();
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkPermission()) {
                Log.e("value", "Permission already Granted.");
                setupData();
            } else {
                requestPermission();
            }
        } else {
            Log.e("value", "Not required for requesting runtime permission");
            setupData();
        }
    }

    public void setupData() {
        myLocation = new MyLocation(this);
        myLocation.getLocation();
        if (!myLocation.checkGpsEnable()) {
            myLocation.showSettingsAlert();
        }
        if (myLocation.checkGpsEnable()) {
            gpsStatus = "1";
        } else {
            gpsStatus = "0";
        }
        if (mySession.getLoginReminder()) {
            startTrackingService();
        }
    }

     /*Animation Method */

    public void Animation() {
        if (Build.VERSION.SDK_INT >= 21) {
            Transition trans = new Explode();
            trans.setDuration(700);
            getWindow().setEnterTransition(trans);
        }
    }

    private void initilization() {
        coordinatorLayout = (RelativeLayout) findViewById(R.id.coordinatorLayout);
        userIdEdit = (EditText) findViewById(R.id.userIdEdit);
        userPassEdit = (EditText) findViewById(R.id.userPassEdit);
        userIdTIL = (TextInputLayout) findViewById(R.id.userIdTIL);
        userPassTIL = (TextInputLayout) findViewById(R.id.userPassTIL);
        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
        signUpCompatButton = (AppCompatButton) findViewById(R.id.submit);
        signUpCompatButton.setOnClickListener(this);
    }

    private boolean checkPermission() {
        int accessFineLocation = ContextCompat.checkSelfPermission(LoginScreenActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
        int accessCourseLocation = ContextCompat.checkSelfPermission(LoginScreenActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION);
        int phoneState = ContextCompat.checkSelfPermission(LoginScreenActivity.this, Manifest.permission.READ_PHONE_STATE);
        if (accessFineLocation == PackageManager.PERMISSION_GRANTED) {
            if (accessCourseLocation == PackageManager.PERMISSION_GRANTED) {
                if (phoneState == PackageManager.PERMISSION_GRANTED) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }

        } else {
            return false;
        }
    }


    private void requestPermission() {
        ActivityCompat.requestPermissions(LoginScreenActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("value", "Permission Granted.");
                    setupData();
                } else {
                    Log.e("value", "Permission Denied.");
                }
                break;
        }
    }

    // Start the service
    public void startTrackingService() {
        if (!checkIfGooglePlayEnabled()) {
            return;
        }

        PushAlarmReceiver.setAlarm(LoginScreenActivity.this, Integer.parseInt(mySession.getIntervalTime()) * 1000 * 60);
        PushAlarmReceiver.setStartimeAlarm(LoginScreenActivity.this);
        if (!CheckServiceIsRunning.isMyServiceRunning(LocationService.class, LoginScreenActivity.this)) {
            startService(new Intent(LoginScreenActivity.this, LocationService.class));
        }
        if (!CheckServiceIsRunning.isMyServiceRunning(CheckLocationServiceStatus.class, LoginScreenActivity.this)) {
            startService(new Intent(LoginScreenActivity.this, CheckLocationServiceStatus.class));
        }
        deviceId = getImeino();
        mySession.SaveStringPref(SessionKeys.ImeiNo, deviceId);
        //PushAlarmReceiver.setStartimeAlarm(LoginScreenActivity.this);
    }

    private boolean checkIfGooglePlayEnabled() {
        if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(this) == ConnectionResult.SUCCESS) {
            return true;
        } else {
            Log.e("Home Page Track EMP :- ", "unable to connect to google play services.");
            Toast.makeText(getApplicationContext(), R.string.google_play_services_unavailable, Toast.LENGTH_LONG).show();
            return false;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public String getImeino() {
        TelephonyManager mngr = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        return mngr.getDeviceId();
    }

    void PushEmpRequest(String locationAddress) {
        if (deviceId.equalsIgnoreCase("")) {
            deviceId = getImeino();
            mySession.SaveStringPref(SessionKeys.ImeiNo, deviceId);
        }
        String latitude = String.format("%1$,.2f", myLocation.getLatitude());
        if (latitude.equalsIgnoreCase("0.00")) {
            myLocation = new MyLocation(LoginScreenActivity.this);
            myLocation.getLocation();
            if (!myLocation.checkGpsEnable()) {
                myLocation.showSettingsAlert();
                myDialog.CancelProgressDialog();
            }
        } else {
            if (CheckInternet.isNetwork(this)) {
                try {
                    Location location = myLocation.getLocation();
                    ArrayList<RequestModel> list = new ArrayList<>();
                    list.add(new RequestModel(WebKeys.push_lat, location.getLatitude() + ""));
                    list.add(new RequestModel(WebKeys.push_long, location.getLongitude() + ""));
                    list.add(new RequestModel(WebKeys.push_gpsstatus, gpsStatus));
                    list.add(new RequestModel(WebKeys.push_device_id, deviceId));
                    list.add(new RequestModel(WebKeys.push_login_id, userIdEdit.getText().toString().trim()));
                    list.add(new RequestModel(WebKeys.push_password, userPassEdit.getText().toString().trim()));
                    list.add(new RequestModel(WebKeys.push_trackAddress, locationAddress));
                    list.add(new RequestModel(WebKeys.push_trackTime, Utils.getTime()));
                    list.add(new RequestModel(WebKeys.push_trackDate, Utils.getDate()));
                    wysRequests.MakeStrRequest(WebKeys.push_key, WebUrls.push_empdetails, list, responceLisnter);
                } catch (Exception e) {
                    e.printStackTrace();
                    myDialog.CancelProgressDialog();
                    showAlert();
                }
            } else {
                myDialog.CancelProgressDialog();
                showAlert();
            }
        }
    }

    private void getServerDateTime() {
        if (CheckInternet.isNetwork(LoginScreenActivity.this)) {
            ArrayList<RequestModel> list = new ArrayList<>();
            //list.add(new RequestModel(WebKeys.Key_UserId, mySession.getUserId()));
            wysRequests.MakeStrRequest(WebKeys.Tag_ServerDateTime, WebUrls.ServerDateTime, list, responceLisnter);
        } else {
            mySession.SaveStringPref(SessionKeys.trackTime, Utils.getTime());
            mySession.SaveStringPref(SessionKeys.trackDate, Utils.getDate());
        }
    }

    public void showAlert() {
        final Dialog dialog = new Dialog(LoginScreenActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.setContentView(R.layout.erroralert);
        Button done = (Button) dialog.findViewById(R.id.submit);
        dialog.setTitle("");
        done.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                myDialog.CancelProgressDialog();
                dialog.cancel();
            }
        });
        dialog.show();
    }

    ResponceLisnter responceLisnter = new ResponceLisnter() {
        @Override
        public void getResponce(String response, String Tag) {
            MyLog.ShowLog(MyConstants.Result, response);
            if (Tag.equalsIgnoreCase(WebKeys.push_key)) {
                PushEmpResult(response);
            } else if (Tag.equalsIgnoreCase(WebKeys.Tag_ServerDateTime)) {
                serverDateTime(response);
            }
        }

        @Override
        public void getResponceError(String errorStr, String Tag) {
            MyToast.Smsg(getApplicationContext(), errorStr);
            myDialog.CancelProgressDialog();
        }
    };


    private void serverDateTime(String result) {
        if (result != null) {
            Log.e("Server DateTime Respon", result);
            try {
                JSONObject jsonObject = new JSONObject(result);
                String DateTimeTaken = jsonObject.getString("time");
                String currentDateTime = jsonObject.getString("date");
                mySession.SaveStringPref(SessionKeys.trackTime, DateTimeTaken);
                mySession.SaveStringPref(SessionKeys.trackDate, currentDateTime);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void PushEmpResult(String response) {
        myDialog.CancelProgressDialog();
        try {
            JSONObject obj = new JSONObject(response);
            String status = obj.optString(WebKeys.Status);
            if (status.equals("1")) {
                mySession.SaveStringPref(SessionKeys.Password, userPassEdit.getText().toString().trim());
                mySession.SaveStringPref(SessionKeys.LoginId, userIdEdit.getText().toString().trim());
                mySession.SaveBooleanPref(SessionKeys.LoginReminder, true);

                String startTime = obj.optString("startTime");
                String endTime = obj.optString("endTime");
                String serverTime = obj.optString("serverTime");
                String interval = obj.optString("Interval");
                String dutyStatus = obj.optString("message");

                mySession.SaveStringPref(SessionKeys.StartTime, startTime);
                mySession.SaveStringPref(SessionKeys.EndTime, endTime);
                mySession.SaveStringPref(SessionKeys.ServerTime, serverTime);
                mySession.SaveStringPref(SessionKeys.IntervalTime, interval);
                mySession.SaveStringPref(SessionKeys.DutyStatus, dutyStatus);

                if (Build.VERSION.SDK_INT >= 23) {
                    if (checkPermission()) {
                        Log.e("value", "Permission already Granted.");
                        setupData();
                    } else {
                        requestPermission();
                    }
                } else {
                    Log.e("value", "Not required for requesting runtime permission");
                    setupData();
                }
                Intent homePageIntent = new Intent(LoginScreenActivity.this, HomePageActivity.class);
                startActivity(homePageIntent);
                finish();
            } else {
                Toast.makeText(getApplicationContext(), obj.optString("message"), Toast.LENGTH_LONG).show();
                mySession.SaveBooleanPref(SessionKeys.LoginReminder, false);
                mySession.SaveStringPref(SessionKeys.DutyStatus, obj.optString("message"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.submit:
                if (validation()) {
                    if (!myLocation.checkGpsEnable()) {
                        myLocation.showSettingsAlert();
                    } else {
                        myDialog.ShowProgressDialog();
                        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                        getServerDateTime();
                        if (CheckInternet.isNetwork(this)) {
                            LocationAddress locationAddress = new LocationAddress();
                            locationAddress.getAddressFromLocation(myLocation.getLatitude(), myLocation.getLongitude(),
                                    LoginScreenActivity.this, new GeocoderHandler());
                        } else {
                            showAlert();
                        }
                    }
                }
                break;
        }
    }

    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationAddress;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getString("address");
                    PushEmpRequest(locationAddress);
                    break;
                default:
                    locationAddress = null;
            }
        }
    }

    private boolean validation() {
        if (userIdEdit.getText().toString().trim().length() == 0) {
            userIdTIL.setError("Please Enter Login Id");
            userIdEdit.requestFocus();
            return false;
        } else if (userPassEdit.getText().toString().trim().length() == 0) {
            userPassTIL.setError("Enter the Valid password");
            userPassEdit.requestFocus();
            return false;
        }
        return true;
    }

    public static void setSnackBar(View coordinatorLayout, String snackTitle) {
        Snackbar snackbar = Snackbar.make(coordinatorLayout, snackTitle, Snackbar.LENGTH_SHORT);
        snackbar.show();
        View view = snackbar.getView();
        TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        txtv.setGravity(Gravity.CENTER_HORIZONTAL);
    }

}
